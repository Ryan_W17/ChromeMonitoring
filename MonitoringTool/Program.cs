﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;

namespace MonitoringTool
{
    internal class Program
    {

        private static List<String> trustedProcess = new List<string>();
        private static bool nicStatus = true;

        public static void Main(string[] args)
        {
            enableAuditing(@"C:\Users\" + Environment.UserName + @"\AppData\Local\Google\Chrome\User Data\Default");
            enableAuditing(@"C:\Users\" + Environment.UserName + @"\AppData\Roaming\Mozilla\Firefox\Profiles");
            trustedProcess.Add("mcshield");
            //trustedProcess.Add("MonitoringTool");
            AttachWatcher();
            for (int i = 0; i < 5; i++)
            {
                Thread.Sleep(10000);
            }
        }


        private static void enableAuditing(string directoryPath)
        {
            Console.WriteLine(">> Attempting to enable auditing for " + directoryPath + " directory....");
            var account = new SecurityIdentifier(WellKnownSidType.WorldSid, null).Translate(typeof(NTAccount));
            DirectorySecurity dSecurity = Directory.GetAccessControl(directoryPath, AccessControlSections.Audit);
            dSecurity.AddAuditRule(new FileSystemAuditRule(account,
                FileSystemRights.ReadData | FileSystemRights.WriteData | FileSystemRights.Delete |
                FileSystemRights.ChangePermissions,
                InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                PropagationFlags.None,
                AuditFlags.Success));
            Directory.SetAccessControl(directoryPath, dSecurity);
            Console.WriteLine(">> Enabled Auditing for Path: " + directoryPath);
        }


        private static void AttachWatcher()
        {
            EventLogQuery logQuery = new EventLogQuery("Security", PathType.LogName, "*[System[(EventID = 4656)]]");
            EventLogWatcher logWatcher = new EventLogWatcher(logQuery);
            logWatcher.EventRecordWritten += new EventHandler<EventRecordWrittenEventArgs>(EventWritten);
            logWatcher.Enabled = true;
        }

        private static void EventWritten(Object obj, EventRecordWrittenEventArgs arg)
        {
            IList<EventProperty> eProp = arg.EventRecord.Properties;

            if (eProp[6].Value.ToString().Contains(@"\Chrome\User Data\Default\"))
            {


                if (!eProp[15].Value.ToString().Contains("chrome") && !isValidExplorer(eProp[15].Value.ToString()))
                {
                    foreach (var trusted in trustedProcess)
                    {
                        if (eProp[15].Value.ToString().Contains(trusted))
                        {
                            return;
                        }
                    }

                    Console.WriteLine(
                        "###########################################################################################");
                    Console.WriteLine(">> ACCESS OF CHROME DIRECTORY FROM EXTERNAL PROCESS <<");
                    Console.WriteLine(">> Process: " + eProp[15].Value + " is accessing the Chrome Directory");
                    Console.WriteLine(
                        "###########################################################################################");
                    disableNetwork();
                    Console.WriteLine(">> DISABLED ALL NETWORK CONNECTIONS TO PREVENT LOSS OF SENSITIVE DATA");
                    Console.ReadLine();
                    return;
                }
            }
            else if (eProp[6].Value.ToString().Contains(@"\AppData\Roaming\Mozilla\FireFox"))
            {
                if (!eProp[15].Value.ToString().Contains("firefox") && !isValidExplorer(eProp[15].Value.ToString()))
                {
                    foreach (var trusted in trustedProcess)
                    {
                        if (eProp[15].Value.ToString().Contains(trusted))
                        {
                            return;
                        }
                    }

                    Console.WriteLine(
                        "###########################################################################################");
                    Console.WriteLine(">> ACCESS OF FIREFOX DIRECTORY FROM EXTERNAL PROCESS <<");
                    Console.WriteLine(">> Process: " + eProp[15].Value + " is accessing the Firefox Directory");
                    Console.WriteLine(
                        "###########################################################################################");
                    disableNetwork();
                    Console.WriteLine(">> DISABLED ALL NETWORK CONNECTIONS TO PREVENT LOSS OF SENSITIVE DATA");
                    Console.ReadLine();
                    return;
                }
            }


        }


        private static void disableNetwork()
        {
            if (!nicStatus)
            {
                return;
            }

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                string nicName = nic.Name;
                ProcessStartInfo psi =
                    new ProcessStartInfo("netsh",
                        "interface set interface \"" + nicName + "\" disable");
                Process p = new Process();
                p.StartInfo = psi;
                p.Start();
            }

            nicStatus = false;
        }

        private static bool isValidExplorer(string checkPath)
        {
            return checkPath.Equals(@"C:\Windows\explorer.exe");
        }
    }
}